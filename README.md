# Challenge 1 - Mardi 4 juillet

Durée : 30 mins.

## Consignes

À partir du travail réalisé hier, créer un package Python qui devra contenir :
- le module de calcul de distances développé la veille
- l’interface CLI développée
- un entrypoint nommé `distance_computor`.


Convention de nommage:
- le package sera nommé `distances`
- la fonction `hausdorff` sera exportée et permet le calcul de distance de Hausdorff
- la fonction `hausdorff` aura pour signature `(np.array, np.array)->float`
- idem pour l’autre fonction qui sera nommée `minimal`.

Bon courage !
